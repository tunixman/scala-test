// The Free Algebra of clothing items.

package rewards

import scala.language.higherKinds
import scala.language.existentials
import shapeless._

package temperature {
  // This module describes the Phantom Type for Temperatures. The
  // Clothing free algebra is parameterized over this to ensure at the
  // type level that the correct clothing items and rules are used.

  // The temperature.
  sealed trait Temperature
  sealed trait Hot extends Temperature
  sealed trait Cold extends Temperature
}

package wearing {
  // This is the phantom type representing whether a clothing item is
  // worn or not.

  sealed trait Wearing
  sealed trait Donned extends Wearing
  sealed trait Doffed extends Wearing
}

package object clothing {
  import temperature._
  import wearing._

  // A clothing item parameterized over Temperature and Wearing.
  sealed trait Clothing[-T, -W]

  // A top clothing item.
  sealed trait TopT[-T, -W] extends Clothing[T, W]

  // a bottom clothing item.
  sealed trait BottomT[-T, -W] extends Clothing[T, W]

  // The Shirt works for both temperatures.
  sealed class Shirt[-T, -W] extends TopT[T, W]

  sealed trait HeadwearT[-T, -W] extends TopT[T, W]
  sealed class Sunglasses[Hot, -W] extends HeadwearT[Hot, W]
  sealed class Hat[Cold, -W] extends HeadwearT[Cold, W]

  // The Jacket is for Cold only.
  sealed class Jacket[Cold, -W] extends TopT[Cold, W]

  // The Pants
  sealed trait PantsT[-T, -W] extends BottomT[T, W]
  sealed class Shorts[Hot, -W] extends PantsT[Hot, W]
  sealed class Pants[Cold, -W] extends PantsT[Cold, W]

  // Socks are only for cold.
  sealed class Socks[Cold, -W] extends BottomT[Cold, W]

  // The footwear
  sealed trait FootwearT[-T, -W] extends BottomT[T, W]
  sealed class Sandals[Hot, -W] extends FootwearT[Hot, W]
  sealed class Boots[Cold, -W] extends FootwearT[Cold, W]
}

package object outfits {
  import temperature._
  import wearing._
  import clothing._

  // The PJs which have to be removed.
  sealed case class PJs[-T, -W]()

  // Fully dressed Outfit, with a Top and a Bottom.
  sealed case class Dressed[-T, -W](d : TopD[T, _] :: BottomD[T, _] :: HNil)

  // A top outfit.
  sealed trait TopD[-T, -W]

  // A bottom outfit.
  sealed trait BottomD[-T,-W]

  type L[+H, +T <: HList] = shapeless.::[H, T]
  type N = shapeless.HNil

  // The Shirt has to be done first, So it's in an outer group.
  sealed class HotTop[Hot, -W] extends TopD[Hot, W] {
    def apply() :
        L[Shirt[Hot, _],
          L[L[HeadwearT[Hot, _],N],N]] =
        new Shirt[Hot, Doffed] :: (new Sunglasses[Hot, Doffed] :: HNil) :: HNil
  }

  // For the cold top, we also need a Jacket. We represent that the
  // jacket and headwear can be in any order by putting them in the
  // same HList, but they must be done after the Shirt.
  sealed case class ColdTop[Cold, -W](
    t : Shirt[Cold, _] :: (HeadwearT[Cold, _] :: Jacket[Cold, _] :: HNil) :: HNil)
      extends TopD[Cold, W]

  // The hot bottom is just sandals and shorts, but again, Pants first.
  sealed case class HotBottom[Hot, -W]
    (b : Pants[Hot, _] :: (FootwearT[Hot, _] :: HNil) :: HNil)
      extends BottomD[Hot, W]

  // The cold bottom is pants, socks, and boots.
  sealed case class ColdBottom[Cold, W](
    b : Pants[Cold, _] :: Socks[Cold, _] :: (FootwearT[Cold, _] :: HNil) :: HNil)
      extends BottomD[Cold, W]
}
