import Dependencies._

lazy val root = (project in file(".")).
  settings(
    inThisBuild(List(
      organization := "com.theunixman",
      scalaVersion := "2.12.1",
      version      := "0.1.0-SNAPSHOT",
      scalacOptions ++= Seq("-feature")
    )),
    resolvers ++= Seq(
      Resolver.sonatypeRepo("releases"),
      Resolver.sonatypeRepo("snapshots")
    ),
    addCompilerPlugin("org.scalamacros" % "paradise" % "2.1.0" cross CrossVersion.full),
    libraryDependencies ++= Seq(
      scalaTest % Test,
      "org.scalacheck" %% "scalacheck" % "1.13.4" % "test",
      "org.scalameta" %% "scalameta" % "1.8.0",
      "org.scalameta" %% "contrib" % "1.8.0",
      "com.chuusai" %% "shapeless" % "2.3.2",
      "org.scalaz" %% "scalaz-core" % "7.2.14",
      "org.typelevel" %% "cats-core" % "1.0.0-MF",
      "org.typelevel" %% "cats-free" % "1.0.0-MF",
      "org.typelevel" %% "cats-effect" % "0.4",
      "org.typelevel" %% "cats-mtl-core" % "0.0.2",
      "org.typelevel" %% "cats-laws" % "1.0.0-MF",
      "org.typelevel" %% "cats-testkit" % "1.0.0-MF"
    ),
    (fullClasspath in Test) ++= {
      (updateClassifiers in Test).value
        .configurations
        .find(_.configuration == Test.name)
        .get
        .modules
        .flatMap(_.artifacts)
        .collect{case (a, f) if a.classifier == Some("sources") => f}
    }
  )
